//
// Created by otavio on 24/12/23.
//

#ifndef XLIGHTS_UTIL_H
#define XLIGHTS_UTIL_H

#include <stdint.h>

void delay_config(void);
void delay(uint32_t ms);
uint32_t get_counter(void);

#endif //XLIGHTS_UTIL_H
