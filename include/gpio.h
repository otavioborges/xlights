//
// Created by otavio on 19/12/23.
//

#ifndef XLIGHTS_GPIO_H
#define XLIGHTS_GPIO_H

typedef enum {
    GPIO_IN_ANALOG = 0,
    GPIO_IN_FLOAT  = 1,
    GPIO_IN_PULL   = 2
}gpio_in_t;

typedef enum {
    GPIO_OUT_SLOW   = 1,
    GPIO_OUT_NORMAL = 2,
    GPIO_OUT_FAST   = 3
}gpio_out_t;

typedef enum {
    GPIO_PUSH_PULL  = 0,
    GPIO_OPEN_DRAIN = 1
}gpio_type_t;

#include "stm32f103xb.h"

void gpio_clear(GPIO_TypeDef *port, int pin);
void gpio_set_input(GPIO_TypeDef *port, int pin, gpio_in_t mode);
void gpio_set_output(GPIO_TypeDef *port, int pin, gpio_out_t mode, gpio_type_t type);
void gpio_set_af(GPIO_TypeDef *port, int pin, gpio_out_t mode, gpio_type_t type);
uint32_t gpio_get(GPIO_TypeDef *port, int pin);
void gpio_set(GPIO_TypeDef *port, int pin);
void gpio_reset(GPIO_TypeDef *port, int pin);

#endif //XLIGHTS_GPIO_H
