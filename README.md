# Xlights - Neopixel Christmas Lights

This a simple baremetal project, based on STMicroeletronics Nucle-F103FB board,
to control a WS2812B LED strip.

## Pre-requisites:

 * arm-gnu-toolchain-12.3.rel1-x86_64-arm-none-eabi;
 * make;
 * autoconf/autotools (2.71 or greater).

## About the code

Source consists of mainly 4 blocks:

 * **xxx_stm32f1xxx.x** - low-level MCU initialization library (from [STM32CubeF1](https://www.st.com/en/embedded-software/stm32cubef1.html));
 * **gpio** - Configures and controls GPIOs;
 * **neopixel** - Library for controlling WS2812B through PWM and DMA;
 * **patterns** - higher level logic for LED patterns, using neopixel.c functions.

## WS2812B (aka Neopixel)

Those consists of addressable LEDs where the proper displayed colour must be
set by sending a series of pulses encoding a 24bit RGB value (as seen at
[WS2812B-Datasheet](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf)).

# Building

Code uses autoconf to perform linking and Makefiles. One does need a arm-none-eabi
toolchain to build a binary that will properly execute on STM32F103RB core.

Environment variable must be set wherever configuration is performed:

 * CC
 * CXX
 * AR
 * AS
 * LD
 * NM
 * OBJCOPY
 * OBJDUMP
 * RANLIB
 * STRIP
 * LDFLAGS

To build the project:

```
$ cd xlights
$ autoreconf -vfi
$ mkdir build
$ cd build
$ ../configure --host=arm-none-eabi --build=$(gcc -dumpmachine)
$ make
```

The binary *xlights* will be created on *build* folder upload that to the board
(by using openOCD, J-Link or other debugger).
