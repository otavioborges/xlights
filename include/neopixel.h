//
// Created by otavio on 19/12/23.
//

#ifndef XLIGHTS_NEOPIXEL_H
#define XLIGHTS_NEOPIXEL_H

#define NEOPIXEL_LEDS        100

#include <stdint.h>

int neopixel_init(void);
void neopixel_fill_color(uint32_t color);
void neopixel_singular_color(int idx, uint32_t color);
void neopixel_transform_color(uint32_t *array);
void neopixel_color_walk(void);
void neopixel_trigger_dma(void);
int neopixel_idle(void);
void neopixel_breath(int cycles, int start_up);

#endif //XLIGHTS_NEOPIXEL_H
