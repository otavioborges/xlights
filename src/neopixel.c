//
// Created by otavio on 19/12/23.
//
#include <string.h>
#include "neopixel.h"
#include "gpio.h"
#include "util.h"
#include "stm32f103xb.h"

#define NEOPIXEL_PSC            0
#define NEOPIXEL_CNT            60
#define NEOPIXEL_RST_MULT       42

#define NEOPIXEL_ONE            38
#define NEOPIXEL_ZERO           19
#define NEOPIXEL_DMA_LENGTH     ((NEOPIXEL_LEDS * 24) + 1)
#define NEOPIXEL_BREATH_DELAY   10

static uint32_t raw_color_array[NEOPIXEL_LEDS];
static uint32_t color_array[NEOPIXEL_DMA_LENGTH];
static uint32_t dma_zeroes[NEOPIXEL_RST_MULT];
static uint32_t pending_reset = 0, occupied = 0;

int neopixel_init(void) {
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPAEN;
    AFIO->MAPR &= ~AFIO_MAPR_TIM1_REMAP;
    AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;
    gpio_clear(GPIOA, 9);
    gpio_set_af(GPIOA, 9, GPIO_OUT_FAST, GPIO_PUSH_PULL);
//    gpio_set_output(GPIOA, 9, GPIO_OUT_FAST, GPIO_PUSH_PULL);

    memset(color_array, 0, (sizeof(uint32_t) * NEOPIXEL_DMA_LENGTH));
    memset(raw_color_array, 0, (sizeof(uint32_t) * NEOPIXEL_LEDS));

    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    TIM1->DMAR = 0;
    TIM1->CR1 = TIM_CR1_URS;
    TIM1->CR2 = TIM_CR2_CCDS;
    TIM1->SMCR = 0;
    TIM1->DIER = TIM_DIER_UDE;
    TIM1->SR = 0;
    TIM1->EGR = 0;

    TIM1->CCER = 0;
    TIM1->CCMR1 = (6 << TIM_CCMR1_OC2M_Pos);
    TIM1->CCER = TIM_CCER_CC2E;
    TIM1->BDTR = TIM_BDTR_MOE;

    /* TIM1 freq is 769,230Hz, for neopixel */
    TIM1->PSC = NEOPIXEL_PSC;
    TIM1->ARR = NEOPIXEL_CNT;
    TIM1->CCR2 = 0;

    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
    DMA1->ISR = 0;
    NVIC_EnableIRQ(DMA1_Channel5_IRQn);
}

void neopixel_fill_color(uint32_t color) {
    int idx;

    for (idx = 0; idx < NEOPIXEL_LEDS; idx++)
        raw_color_array[idx] = color;

    neopixel_trigger_dma();
}

int neopixel_idle(void) {
    if (occupied)
        return 0;
    else
        return 1;
}

void neopixel_breath(int cycles, int start_up) {
    int idx, pixel;
    uint32_t _colors[NEOPIXEL_LEDS];

    memcpy(_colors, raw_color_array, (sizeof(uint32_t) * NEOPIXEL_LEDS));
    if (start_up) {
        memset(raw_color_array, 0, (sizeof(uint32_t) * NEOPIXEL_LEDS));
        start_up = 0xff;
    }

    for (idx = 0; idx < (cycles * 0xff); idx++) {
        for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
            if (start_up > 0) {
                if ((raw_color_array[pixel] & 0xff0000) < (_colors[pixel] & 0xff0000))
                    raw_color_array[pixel] += 0x010000;

                if ((raw_color_array[pixel] & 0xff00) < (_colors[pixel] & 0xff00))
                    raw_color_array[pixel] += 0x0100;

                if ((raw_color_array[pixel] & 0xff) < (_colors[pixel] & 0xff))
                    raw_color_array[pixel] += 0x01;
            } else {
                if ((raw_color_array[pixel] & 0xff0000))
                    raw_color_array[pixel] -= 0x010000;

                if ((raw_color_array[pixel] & 0xff00))
                    raw_color_array[pixel] -= 0x0100;

                if ((raw_color_array[pixel] & 0xff))
                    raw_color_array[pixel] -= 0x01;
            }
        }

        start_up--;
        if (start_up <= -0xff)
            start_up = 0xff;

        while (neopixel_idle() == 0)
            delay(5);

        neopixel_trigger_dma();
        delay(NEOPIXEL_BREATH_DELAY);
    }
}

#if 0
void neopixel_legacy () {
    int idx, step = (0xFF / NEOPIXEL_BREATH_RATE), pixel;
    uint32_t colors[NEOPIXEL_LEDS];

    neopixel_transform_color(colors);
    for (idx = 0; idx < step; idx++) {
        for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
            if ((colors[pixel] & 0xff0000) > (NEOPIXEL_BREATH_RATE << 16))
                colors[pixel] -= (NEOPIXEL_BREATH_RATE << 16);
            else
                colors[pixel] &= 0x00ffff;

            if ((colors[pixel] & 0x00ff00) > (NEOPIXEL_BREATH_RATE << 8))
                colors[pixel] -= (NEOPIXEL_BREATH_RATE << 8);
            else
                colors[pixel] &= 0xff00ff;

            if ((colors[pixel] & 0x0000ff) > NEOPIXEL_BREATH_RATE)
                colors[pixel] -= NEOPIXEL_BREATH_RATE;
            else
                colors[pixel] &= 0xffff00;

            neopixel_singular_color(pixel, colors[pixel]);
        }

        while (neopixel_idle() == 0)
            delay(10);

        neopixel_trigger_dma();
        delay(50);
    }
}
#endif

void neopixel_trigger_dma(void) {
    int bit, pixel;

    pending_reset = 1;
    occupied = 1;

    for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
        for (bit = 23; bit >= 0; bit--) {
            if ((raw_color_array[pixel] >> bit) & 1)
                color_array[((pixel * 24) + (23 - bit))] = NEOPIXEL_ONE;
            else
                color_array[((pixel * 24) + (23 - bit))] = NEOPIXEL_ZERO;
        }
    }

    DMA1_Channel5->CCR = DMA_CCR_PL | DMA_CCR_MSIZE_1 | DMA_CCR_PSIZE_1 | DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TEIE | DMA_CCR_TCIE;
    DMA1_Channel5->CNDTR = NEOPIXEL_DMA_LENGTH;

    DMA1_Channel5->CPAR = (uint32_t)(&(TIM1->DMAR));
    DMA1_Channel5->CMAR = (uint32_t)(color_array);

    TIM1->DCR = 14;
    TIM1->DIER |= TIM_DIER_UDE;
    TIM1->CR1 = TIM_CR1_CEN;
    DMA1_Channel5->CCR |= DMA_CCR_EN;
}

void neopixel_singular_color(int idx, uint32_t color) {
    raw_color_array[idx] = color;
}

void neopixel_transform_color(uint32_t *array) {
    int idx, color_idx = 0, offset = 23;
    uint32_t current_color = 0;

    for(idx = 0; idx < NEOPIXEL_DMA_LENGTH; idx++) {
        if (color_array[idx] == NEOPIXEL_ONE)
            current_color |= (1 << offset);

        offset--;
        if (offset < 0) {
            array[color_idx] = current_color;
            color_idx++;
            current_color = 0;
            offset = 23;
        }
    }
}

void neopixel_color_walk(void) {
    int pixel;
    uint32_t pixel0;

    pixel0 = raw_color_array[NEOPIXEL_LEDS - 1];
    for (pixel = NEOPIXEL_LEDS - 1; pixel >= 0; pixel--) {
        if (pixel != 0)
            raw_color_array[pixel] = raw_color_array[(pixel - 1)];
        else
            raw_color_array[0] = pixel0;
    }
}

void DMA1_Channel5_IRQHandler(void) {
    DMA1->IFCR = DMA_IFCR_CGIF5;
    TIM1->CR1 &= ~TIM_CR1_CEN;

    if (pending_reset) {
        DMA1_Channel5->CCR = DMA_CCR_PL | DMA_CCR_MSIZE_1 | DMA_CCR_PSIZE_1 | DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TEIE | DMA_CCR_TCIE;
        DMA1_Channel5->CNDTR = NEOPIXEL_RST_MULT;
        pending_reset = 0;

        DMA1_Channel5->CPAR = (uint32_t)(&(TIM1->DMAR));
        DMA1_Channel5->CMAR = (uint32_t)(dma_zeroes);

        TIM1->DCR = 14;
        TIM1->DIER |= TIM_DIER_UDE;
        TIM1->CR1 = TIM_CR1_CEN;
        DMA1_Channel5->CCR |= DMA_CCR_EN;
    } else {
        occupied = 0;
    }
}
