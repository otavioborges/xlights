//
// Created by otavio on 24/12/23.
//

#include "stm32f103xb.h"

static uint32_t _count = 0;

void delay_config(void) {
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);
}

void delay(uint32_t ms) {
    ms += _count;
    while(_count > ms)
        __asm("nop");

    while(_count < ms)
        __asm("nop");
}

uint32_t get_counter(void) {
    return _count;
}

void SysTick_Handler (void) {
    _count++;
}
