//
// Created by otavio on 24/12/23.
//

#ifndef XLIGHTS_PATTERNS_H
#define XLIGHTS_PATTERNS_H

typedef enum {
    COUNTRY_BRAZIL,
    COUNTRY_CANADA
}country_t;

void pattern_breath(void);
void pattern_color_pulse(void);
void pattern_color_wave(void);
void pattern_rnd_catch(void);
void pattern_rnd_catch(void);
void pattern_blink(void);
void pattern_country_mode(country_t country);
void pattern_snake (int size);
void pattern_solo_snake(int size);

#endif //XLIGHTS_PATTERNS_H
