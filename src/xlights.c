#include <stdio.h>

#include "stm32f103xb.h"
#include "gpio.h"
#include "neopixel.h"
#include "patterns.h"
#include "util.h"

#define STEP_TIME    60000

static void rcc_setup(void);

void main(void) {
    int step = 0, current_count = 0;

    rcc_setup();
    delay_config();

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    gpio_clear(GPIOA, 5);
    gpio_set_output(GPIOA, 5, GPIO_OUT_NORMAL, GPIO_PUSH_PULL);
    gpio_reset(GPIOA, 5);
    neopixel_init();

    current_count = get_counter() + STEP_TIME;
    for(;;) {
        if (neopixel_idle() == 0) {
            delay(5);
            continue;
        }

        switch (step) {
            case 0:
                pattern_breath();
                break;
            case 1:
                pattern_color_pulse();
                break;
            case 2:
                pattern_color_wave();
                break;
            case 3:
                pattern_rnd_catch();
                delay(50);
                break;
            case 4:
                pattern_blink();
                delay(50);
                break;
            case 5:
                pattern_country_mode(COUNTRY_BRAZIL);
                break;
            case 6:
                pattern_country_mode(COUNTRY_CANADA);
                break;
            case 7:
                pattern_snake(32);
                break;
            case 8:
                pattern_solo_snake(32);
                break;
        }

        if (current_count < get_counter()) {
            current_count = get_counter() + STEP_TIME;
            step++;
            if (step > 8)
                step = 0;

            neopixel_breath(1, 0);
            delay(500);
        }
    }
}

static void rcc_setup(void) {
    uint32_t temp;
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    RCC->CR = RCC_CR_HSION;
    while ((RCC->CR & RCC_CR_HSIRDY) == 0)
        __asm("nop");

    RCC->CR |= RCC_CR_CSSON;
    RCC->CFGR = (10 << RCC_CFGR_PLLMULL_Pos) | (2 << RCC_CFGR_ADCPRE_Pos) | (4 << RCC_CFGR_PPRE1_Pos);
    RCC->CR |= RCC_CR_PLLON;
    while ((RCC->CR & RCC_CR_PLLRDY) == 0)
        __asm("nop");

    temp = RCC->CFGR;
    temp &= ~RCC_CFGR_SW;
    temp |= RCC_CFGR_SW_PLL;
    RCC->CFGR = temp;
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL)
        __asm("nop");

    /* No RTC, configure the motherf*cker */
    if ((RCC->BDCR & RCC_BDCR_RTCEN) == 0) {
        RCC->APB1ENR |= RCC_APB1ENR_PWREN;
        PWR->CR |= PWR_CR_DBP;

        RCC->BDCR |= RCC_BDCR_BDRST;
        for (temp = 0; temp < 0xffff; temp++)
            __asm("nop");

        RCC->BDCR &= ~RCC_BDCR_BDRST;
        RCC->BDCR |= RCC_BDCR_LSEON;
        while ((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY)
            __asm("nop");

        temp = RCC->BDCR;
        temp &= ~(RCC_BDCR_RTCSEL);
        temp |= RCC_BDCR_RTCSEL_LSE;
        RCC->BDCR = temp;

        RCC->BDCR |= RCC_BDCR_RTCEN;
        PWR->CR &= PWR_CR_DBP;
    }
}

void HardFault_Handler (void) {
    __asm("bkpt");
}
