//
// Created by otavio on 19/12/23.
//
#include "gpio.h"

void gpio_clear(GPIO_TypeDef *port, int pin) {
    if (pin <= 7)
        port->CRL &= ~(0xF << (pin << 2));
    else
        port->CRH &= ~(0xF << ((pin - 8) << 2));
}

void gpio_set_input(GPIO_TypeDef *port, int pin, gpio_in_t mode) {
    if (pin <= 7)
        port->CRL |= mode << ((pin << 2) + 2);
    else
        port->CRH |= mode << (((pin - 8) << 2) + 2);
}

void gpio_set_output(GPIO_TypeDef *port, int pin, gpio_out_t mode, gpio_type_t type) {
    if (pin <= 7)
        port->CRL |= ((type << 2) + mode) << (pin << 2);
    else
        port->CRH |= ((type << 2) + mode) << ((pin - 8) << 2);
}

void gpio_set_af(GPIO_TypeDef *port, int pin, gpio_out_t mode, gpio_type_t type) {
    if (pin <= 7)
        port->CRL |= (((type | 0x2) << 2) + mode) << (pin << 2);
    else
        port->CRH |= (((type | 0x2) << 2) + mode) << ((pin - 8) << 2);
}

uint32_t gpio_get(GPIO_TypeDef *port, int pin) {
    return ((port->IDR >> pin) & 1);
}

void gpio_set(GPIO_TypeDef *port, int pin) {
    port->BSRR |= 1 << pin;
}

void gpio_reset(GPIO_TypeDef *port, int pin) {
    port->BSRR |= 1 << (pin + 16);
}
