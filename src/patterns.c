//
// Created by otavio on 24/12/23.
//
#include "stm32f103xb.h"
#include "neopixel.h"
#include "util.h"
#include "patterns.h"
#include <stdlib.h>

#define WALK_SPEED 50

/* Colorspace is BGR */
void pattern_breath(void) {
    int pixel;
    uint32_t seed = RTC->CNTH;
    seed = (seed << 16) + RTC->CNTL;

    srandom(seed);
    uint32_t color = rand() & 0xffffff;
    switch (rand() % 4) {
        case 0:
            color &= 0xff00ff;
            break;
        case 1:
            color &= 0xffff;
            break;
        case 2:
            color &= 0xffff00;
            break;
    }

    for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++)
        neopixel_singular_color(pixel, color);

    neopixel_breath(7, 1);
}

void pattern_color_pulse(void) {
    uint32_t ziColor = 0x0;
    uint8_t red = 0xff, green = 0, blue = 0, stage = 1;
    while(stage < 4) {
        if (neopixel_idle()) {
            switch (stage) {
                case 1:
                    red--;
                    green++;
                    if (red == 0)
                        stage++;
                    break;
                case 2:
                    green--;
                    blue++;
                    if (green == 0)
                        stage++;
                    break;
                case 3:
                    blue--;
                    red++;
                    if (blue == 0)
                        stage++;
                    break;
            }

            ziColor = (uint32_t)red | ((uint32_t)blue << 16) | ((uint32_t)green << 8);
            neopixel_fill_color(ziColor);
        }
        delay(WALK_SPEED);
    }
}

void pattern_color_wave(void) {
    /* steps are 6 transitions of 255 colors */
    int pixel, step = 1536 / NEOPIXEL_LEDS;
    uint32_t color = 0xff0000, stage = 0, delta = 0;

    if ((1536 % NEOPIXEL_LEDS) != 0)
        step++;

    for(pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
        neopixel_singular_color(pixel, color);

        delta += step;
        switch (stage) {
            case 0:
                /* keep blue, up red */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0xff0000 + delta;
                    stage++;
                    delta = 0;
                } else {
                    color = 0xff0000 + delta;
                }
                break;
            case 1:
                /* keep red, remove blue */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0xff00ff - (delta << 16);
                    stage++;
                    delta = 0;
                } else {
                    color = 0xff00ff - (delta << 16);
                }
                break;
            case 2:
                /* keep red, up green */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0x0000ff + (delta << 8);
                    stage++;
                    delta = 0;
                } else {
                    color = 0x0000ff + (delta << 8);
                }
                break;
            case 3:
                /* keep green, remove red */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0xffff - delta;
                    stage++;
                    delta = 0;
                } else {
                    color = 0xffff - delta;
                }
                break;
            case 4:
                /* keep green, up blue */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0xff00 + (delta << 16);
                    stage++;
                    delta = 0;
                } else {
                    color = 0xff00 + (delta << 16);
                }
                break;
            case 5:
                /* keep blue, remove green */
                if (delta >= 0xFF) {
                    delta = 0xFF;
                    color = 0xffff00 - (delta << 8);
                    stage++;
                    delta = 0;
                } else {
                    color = 0xffff00 - (delta << 8);
                }
                break;
            default:
                color = 0xff0000;
        }
    }

    for (pixel = 0; pixel < (NEOPIXEL_LEDS << 1); pixel++) {
        if (neopixel_idle()) {
            neopixel_color_walk();
            neopixel_trigger_dma();
        } else {
            delay(WALK_SPEED);
        }
    }
}

void pattern_rnd_catch(void) {
    int pixel;

    for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
        neopixel_singular_color(pixel, (rand() & 0xffffff));
    }

    while(neopixel_idle() == 0) {
        delay(10);
    }

    neopixel_trigger_dma();
}

void pattern_blink(void) {
    int pixel, stage;

    for(pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
        stage = rand() % 5;
        switch (stage) {
            case 0:
                neopixel_singular_color(pixel, 0x0000ff);
                break;
            case 1:
                neopixel_singular_color(pixel, 0x00ff00);
                break;
            case 2:
                neopixel_singular_color(pixel, 0xff0000);
                break;
            case 3:
                neopixel_singular_color(pixel, 0x00ffff);
                break;
            case 4:
                neopixel_singular_color(pixel, 0xffff00);
                break;
        }
    }

    while(neopixel_idle() == 0) {
        delay(10);
    }

    neopixel_trigger_dma();
}

void pattern_country_mode(country_t country) {
    uint32_t color;
    int pixel, yellowing = 0, steps, delta = 0;

    if(country == COUNTRY_BRAZIL) {
        color = 0x00ff00;
        steps = 768 / NEOPIXEL_LEDS;
        steps++;
    } else {
        color = 0x0000ff;
        steps = 512 / NEOPIXEL_LEDS;
        steps++;
    }

    for (pixel = 0; pixel < NEOPIXEL_LEDS; pixel++) {
        neopixel_singular_color(pixel, color);

        delta += steps;
        if (country == COUNTRY_BRAZIL) {
            if (yellowing == 0) {
                if (delta >= 0xff) {
                    color = 0xffff;
                    delta = 0;
                    yellowing = 1;
                } else {
                    color = 0xff00 + delta;
                }
            } else if (yellowing == 1) {
                if (delta >= 0xff) {
                    color = 0xff0000;
                    delta = 0;
                    yellowing = 2;
                } else {
                    color = (0x00ffff - (delta | (delta << 8))) + (delta << 16);
                }
            } else if (yellowing == 2) {
                if (delta >= 0xff) {
                    color = 0xff00;
                    delta = 0;
                    yellowing = 3;
                } else {
                    color = (0xff0000 - (delta << 16)) + (delta << 8);
                }
            } else {
                color = 0xff00;
            }
        } else {
            if (yellowing == 0) {
                if (delta >= 0xff) {
                    color = 0xffffff;
                    delta = 0;
                    yellowing = 1;
                } else {
                    color = 0xff + ((delta << 8) | (delta << 16));
                }
            } else if (yellowing == 1) {
                if (delta >= 0xff) {
                    color = 0xff;
                    delta = 0;
                    yellowing = 2;
                } else {
                    color = 0xffffff - ((delta << 8) | (delta << 16));
                }
            } else {
                color = 0xff;
            }
        }
    }

    for (pixel = 0; pixel < (NEOPIXEL_LEDS << 1); pixel++) {
        if (neopixel_idle()) {
            neopixel_color_walk();
            neopixel_trigger_dma();
        } else {
            delay(WALK_SPEED);
        }
    }
}

void pattern_snake (int size) {
    int pixel, step = (0xff / size), delta = 0;
    uint32_t red = rand() & 0xff, green = rand() & 0xff, blue = rand() & 0xff, color;

    int stage = rand() % 5;
    switch (stage) {
        case 0:
            blue = 0;
            green = 0;
            red = 0xff;
            break;
        case 1:
            blue = 0;
            green = 0xff;
            red = 0;
            break;
        case 2:
            blue = 0xff;
            green = 0;
            red = 0;
            break;
        case 3:
            blue = 0;
            green = 0xff;
            red = 0xff;
            break;
        case 4:
            blue = 0xff;
            green = 0;
            red = 0xff;
            break;

    }

    neopixel_fill_color(0xffffff);
    for (pixel = size; pixel >= 0; pixel--) {
        color = (blue << 16) | (green << 8) | red;
        neopixel_singular_color(pixel, color);
        if ((blue + step) < 0xff)
            blue += step;
        else
            blue = 0xff;
        if ((green + step) < 0xff)
            green += step;
        else
            green = 0xff;
        if ((red + step) < 0xff)
            red += step;
        else
            red = 0xff;
    }

    for (pixel = 0; pixel < (NEOPIXEL_LEDS << 1); pixel++) {
        if (neopixel_idle()) {
            neopixel_color_walk();
            neopixel_trigger_dma();
        } else {
            delay(WALK_SPEED);
        }
    }
}

void pattern_solo_snake(int size) {
    int pixel, step = (0xff / size);
    uint32_t red = rand() & 0xff, green = rand() & 0xff, blue = rand() & 0xff, color;

    neopixel_fill_color(0);
    for (pixel = size; pixel >= 0; pixel--) {
        color = (blue << 16) | (green << 8) | red;
        neopixel_singular_color(pixel, color);
        if (blue > step)
            blue -= step;
        else
            blue = 0;
        if (green > step)
            green -= step;
        else
            green = 0;
        if (red > step)
            red -= step;
        else
            red = 0;
    }

    for (pixel = 0; pixel < (NEOPIXEL_LEDS << 1); pixel++) {
        if (neopixel_idle()) {
            neopixel_color_walk();
            neopixel_trigger_dma();
        } else {
            delay(WALK_SPEED);
        }
    }
}
